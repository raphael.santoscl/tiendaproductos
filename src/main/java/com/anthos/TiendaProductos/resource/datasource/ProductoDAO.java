package com.anthos.TiendaProductos.resource.datasource;

import com.anthos.TiendaProductos.resource.datasource.entity.Productos;
import org.springframework.data.repository.CrudRepository;

public interface ProductoDAO extends CrudRepository <Productos,Long> {

}
