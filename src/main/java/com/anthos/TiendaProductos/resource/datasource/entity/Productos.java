package com.anthos.TiendaProductos.resource.datasource.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

@Entity
public class Productos {
@Id
@GeneratedValue
private Long id;
private String NombreProducto;
private int CantidadProducto;

    public Productos(Long id, String nombreProducto, int cantidadProducto) {
        this.id = id;
        NombreProducto = nombreProducto;
        CantidadProducto = cantidadProducto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreProducto() {
        return NombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        NombreProducto = nombreProducto;
    }

    public int getCantidadProducto() {
        return CantidadProducto;
    }

    public void setCantidadProducto(int cantidadProducto) {
        CantidadProducto = cantidadProducto;
    }
}
