package com.anthos.TiendaProductos.resource.repository.agregarproducto;

import com.anthos.TiendaProductos.resource.repository.agregarproducto.impl.AgregarProducto;

public interface AgregarProductoRepository {
    void saveAgregarProducto(AgregarProducto productos);
}
