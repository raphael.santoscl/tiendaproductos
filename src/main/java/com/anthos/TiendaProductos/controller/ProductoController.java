package com.anthos.TiendaProductos.controller;

import com.anthos.TiendaProductos.resource.repository.agregarproducto.impl.AgregarProducto;
import com.anthos.TiendaProductos.usecase.agregarproducto.AgregarProductoUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ProductoController {
@Autowired
    private AgregarProductoUseCase usecaseAgregarProducto;

@PostMapping(path="/save")

    public void usecaseAgregarProducto(@RequestBody AgregarProducto productos){
    usecaseAgregarProducto.execute(productos);
}


}
