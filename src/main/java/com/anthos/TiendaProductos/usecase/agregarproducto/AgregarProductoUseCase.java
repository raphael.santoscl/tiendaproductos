package com.anthos.TiendaProductos.usecase.agregarproducto;


import com.anthos.TiendaProductos.resource.repository.agregarproducto.AgregarProductoRepository;
import com.anthos.TiendaProductos.resource.repository.agregarproducto.impl.AgregarProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AgregarProductoUseCase {

    @Autowired
    private AgregarProductoRepository agregarProductoRepository;


public void execute (AgregarProducto agregarProducto){
agregarProductoRepository.saveAgregarProducto(agregarProducto);
}

}
